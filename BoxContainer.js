import React from 'react';
import { StyleSheet, View } from 'react-native';

const BoxContainer = props => {
    return (
        <View style={{ ...styles.boxContainer, ...props.style }}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    boxContainer: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 4,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        justifyContent: 'center',
        marginRight: 16,
        marginLeft: 16,
        marginTop: 16,
        paddingTop: 10,
        paddingBottom: 30,
    }
});

export default BoxContainer;