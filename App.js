import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ScrollView } from 'react-native';
import BoxContainer from './BoxContainer';
import { Ionicons } from "@expo/vector-icons";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <BoxContainer style={styles.boxContainer}>
            <Text style={styles.headerTitle}>
              Make an impact today
          </Text>
            <Ionicons style={styles.close} name="ios-close-circle" size={25} />
            <Text style={styles.sectionTitle}>
              How it works
          </Text>
            <Text style={styles.sectionSubTitle}>
              Give your Walgreens Cash (TM) Rewards to an non profit of your choice. you'll earn on every time you shop, you can contribute any amount you'd like. It's that easy.
          </Text>
            <Text style={styles.sectionTitle}>
              Every dollar counts
          </Text>
            <Text style={styles.sectionSubTitle}>
              When ever you'd like to contribute $1, $5, or more, you will be making a big difference. It really adds up!
          </Text>
            <View style={styles.outerContainer}>
              <Text style={styles.sectionSubTitle}>
                Thanks to my walgreens members like you. We've raised $400(and counting!) to support our communities.
          </Text>
            </View>
          </BoxContainer>
          <StatusBar style="auto" />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxContainer: {
    backgroundColor: '#fff',
  },
  scrollView: {
    backgroundColor: 'white',
    marginHorizontal: 0,
  },
  headerTitle: {
    fontSize: 27,
    textAlign: 'left',
    fontWeight: 'bold',
    paddingLeft: 10,
    paddingBottom: 10
  },
  sectionTitle: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingBottom: 3
  },
  sectionSubTitle: {
    fontSize: 20,
    textAlign: 'center',
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 12
  },
  close: {
    margin: 5,
    position: "absolute",
    top: 0,
    right: 0,
    width: 25,
    height: 25,
    color: "grey"
  },
  outerContainer: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
    marginLeft: 20,
    paddingLeft: 1,
    paddingRight: 1,
    paddingTop: 3,
  }
});